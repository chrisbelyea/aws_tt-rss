#!/bin/bash

INSTALLPATH="/opt/tt-rss"
SOURCEPATH="s3://ttrss-bootstrap/Tiny-Tiny-RSS-"
TTRSSVER="1.12"
APPPATH=$INSTALLPATH/Tiny-Tiny-RSS-$TTRSSVER

################################
# Install required packages    #
################################
yum update --assumeyes
yum groupinstall --assumeyes "Web Server" "PHP Support"
yum install --assumeyes php-pgsql php-mbstring php-mcrypt
groupadd ttrss
usermod -a -G ttrss apache
chown -R apache:ttrss $INSTALLPATH
chkconfig httpd on
service httpd restart

################################
# Download TT-RSS and deploy   #
################################
mkdir --parents $INSTALLPATH
aws s3 cp $SOURCEPATH$TTRSSVER.tar.gz /tmp/tt-rss.tar.gz
tar --extract --gzip --directory=$INSTALLPATH --file=/tmp/tt-rss.tar.gz
rm /tmp/tt-rss.tar.gz
#mv $INSTALLPATH/Tiny-Tiny-RSS-$TTRSSVER/* $INSTALLPATH/
#rmdir $INSTALLPATH/Tiny-Tiny-RSS-$TTRSSVER
# Serve TT-RSS from the document root (i.e. no '/tt-rss' in the URL)
# This could be improved in the future by modifying httpd.conf to
# use a <Virtual Host> directive
rm -rf /var/www/html
ln --symbolic $APPPATH /var/www/html
