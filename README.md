# TT-RSS CloudFormation Templates
This repository contains CloudFormation templates to deploy Tiny Tiny RSS (TT-RSS) in AWS on EC2 and RDS.

The original template was written in JSON (`TinyTinyRSS.json`) and has been converted to YAML (`TinyTinyRSS.yaml`) for improved readability.

Once the stack is running you can log in as `admin`/`password`. **Be sure to change these credentials immediately!**

## Using
To deploy a stack, run the following command with the AWS CLI, substituting appropriate parameters as needed:

```shell
aws cloudformation create-stack \
  --stack-name <value> \
  --template-body file://TinyTinyRSS.yaml \
  --parameters \
    ParameterKey=KeyName,ParameterValue=<your key> \
    ParameterKey=SshLocation,ParameterValue=0.0.0.0/0 \
    ParameterKey=DbPassword,ParameterValue=<your password>
```
