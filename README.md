# TT-RSS CloudFormation Templates
This repository contains a CloudFormation template (`TinyTinyRSS.yaml`) to deploy Tiny Tiny RSS (TT-RSS) in AWS on EC2 and RDS.

Once the stack is running you can log in as `admin`/`password`. **Be sure to change these credentials immediately!**

## Architecture
The stack is a basic two-tier application with RDS (Multi-AZ optional) for the database and a multi-AZ Autoscaling Group for the web/application layer. A load balancer terminates SSL (using a certificate from ACM/IAM) and distributes the load across the EC2 instances. A Route 53 ALIAS record provides the entry point to the load balancer.

![Architecture Diagram](ttrss_architecture.png)

The architecture could be revised to reduce cost by removing the load balancer and using Route 53 to distribute load across the EC2 instances. [Let's Encrypt](https://letsencrypt.org/) could be used for HTTPS on each instance.

## Using
To deploy a stack, run the following command with the AWS CLI, substituting appropriate parameters as needed:

```shell
aws cloudformation deploy \
  --stack-name <value> \
  --template-body file://TinyTinyRSS.yaml \
  --parameters \
    ParameterKey=KeyName,ParameterValue="<your key>" \
    ParameterKey=SshLocation,ParameterValue="10.0.1.5/32" \
    ParameterKey=SslCertificateArn,ParameterValue="arn:aws:acm:us-east-1:123456789012:certificate/12345678-abcd-ef01-4444-480a063c3ad9" \
    ParameterKey=DbPassword,ParameterValue="<your password>" \
    ParameterKey=HostedZoneName,ParameterValue="<your hosted zone name>" \
    ParameterKey=Vpc,ParameterValue="<your VPC ID>" \
    ParameterKey=Subnets,ParameterValue="<comma-separated list of public subnets>"
  --capabilities CAPABILITY_IAM
```

### Restoring a RDS Snapshot
To deploy a stack using an existing RDS snapshot as the basis for a new RDS instance, add the `RdsSnapshot` parameter:

```shell
aws cloudformation deploy \
  --stack-name <value> \
  --template-body file://TinyTinyRSS.yaml \
  --parameters \
    ParameterKey=KeyName,ParameterValue="<your key>" \
    ParameterKey=SshLocation,ParameterValue="10.0.1.5/32" \
    ParameterKey=SslCertificateArn,ParameterValue="arn:aws:acm:us-east-1:123456789012:certificate/12345678-abcd-ef01-4444-480a063c3ad9" \
    ParameterKey=DbPassword,ParameterValue="<your password>" \
    ParameterKey=HostedZoneName,ParameterValue="<your hosted zone name>" \
    ParameterKey=Vpc,ParameterValue="<your VPC ID>" \
    ParameterKey=Subnets,ParameterValue="<comma-separated list of public subnets>"
    ParameterKey=RdsSnapshot,ParameterValue="<RDS snapshot name/ARN of the TT-RSS database>" \
  --capabilities CAPABILITY_IAM
```
